from socket import *
import time
import random
import string
import struct
import re
import femida_checker
from femida_checker import Result


class SampleChecker(femida_checker.Checker):

    port = 7373

    def get_id(self, flag):
        flag = flag
        fid = 0x00

        fid |= (ord(flag[0]) << 24)
        fid |= (ord(flag[1]) << 16)
        fid |= (ord(flag[2]) << 8)
        fid |= (ord(flag[3]))

        fid &= 0x7fffffff

        return fid

    def _push(self, team_ip, flag_id, flag):
        flag = flag
        flag_id = self.get_id(flag)

        try:
            conn = create_connection((str(team_ip), SampleChecker.port))
            conn.settimeout(10.0)
        except Exception as ex:
            self.logger.error('Failed connect to the service notesup')
            self.logger.debug(str(ex), exc_info=True)
            return (Result.DOWN, flag_id)

        try:
            conn.recv(1000)
            conn.send("@updates")
            conn.send("@push")
            conn.send(struct.pack(">I", flag_id))
            conn.send(flag + '\n')
            conn.recv(1000)
        except Exception as ex:
            print ex
            self.logger.error('Failed to commit the protocol (though the service is responding)')
            self.logger.debug(str(ex), exc_info=True)
            return (Result.MUMBLE, flag_id)
        finally:
            conn.close()

        return (Result.OK, flag_id)


    def _pull(self, team_ip, flag_id, flag):
        flag = flag.decode('utf-8').encode('ascii')
        flag_id = self.get_id(flag)

        try:
            conn = create_connection((str(team_ip), SampleChecker.port))
            conn.settimeout(10.0)
        except Exception as ex:
            self.logger.error('Failed connect to the service')
            self.logger.debug(str(ex), exc_info=True)
            return Result.DOWN

        try:
            conn.recv(1000)
            conn.send("@updates")
            conn.send("@pull")
            conn.send(struct.pack(">I", flag_id))
            retrieved_flag = conn.recv(33)
            if flag != retrieved_flag:
                return Result.CORRUPT
        except Exception as ex:
            self.logger.error('Failed to commit the protocol (though the service is responding)')
            self.logger.debug(str(ex), exc_info=True)
            return Result.MUMBLE
        finally:
            conn.close()

        return Result.OK





#my_checker = SampleChecker()
#my_checker.run()

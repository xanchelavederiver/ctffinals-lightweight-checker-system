#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
import time
import sys
import traceback
from hashlib import md5
import threading
import SocketServer
import femida_checker
from femida_checker import Result
import post
import notesup



#
# global vars
#

page_html_path = 'index.html'
accepter_ip ='0.0.0.0'
accepter_port = 8888
round_number = 0
teams = None
teams_by_ip = {}
services = None
total_flags = []
stolen_flags = []





#
# service status and checker
#

class Service(object):
	def __init__(self, checker_module):
		self.checker = checker_module.SampleChecker()
		#self.status = Result.OK

class Team(object):
	def __init__(self, t_n, t_ip, services):
		self.name = t_n
		self.ip = t_ip
		self.services_statuses = {}
		self.own_flags = {}
		for service in services:
			self.own_flags[service] = []
			self.services_statuses[service] = Result.OK
		self.total_own_flags = []
		self.stolen_flags = []
		self.deffence = -1
		self.attack = -1
		self.rank = -1

class Flag(object):
	def __init__(self, f_v, f_id):
		self.value = f_v
		self.id = f_id
		self.is_stolen = False





#
# Push and pull loop, accepting flag
#

def PushPullLoop():
	def generate_flag(flag_id):
		m = md5()
		m.update(str(random.randint(0, 10000000000)) + str(random.randint(0, 10000000000)))
		flag_value = m.hexdigest() + '='
		return Flag(flag_value, flag_id)
	def Push(round_number, service, team):
		f = generate_flag(round_number)
		(team.services_statuses[service], f.id) = service.checker._push(team.ip, f.id, f.value)
		if team.services_statuses[service] == Result.OK:
			team.own_flags[service].append(f)
			team.total_own_flags.append(f.value)
			total_flags.append(f.value)
		else:
			team.own_flags[service].append(None)
	def Pull(round_number, service, team):
		f = team.own_flags[service][round_number]
		if f is not None:
			team.services_statuses[service] = service.checker._pull(team.ip, f.id, f.value)
		else:
			team.services_statuses[service] = Result.DOWN

	round_number = 0
	while True:
		try:
			print '\nRound ', str(round_number)

			# put flags
			for service in services:
				for team in teams:
					f = generate_flag(round_number)
					(team.services_statuses[service], f.id) = service.checker._push(team.ip, f.id, f.value)
					if team.services_statuses[service] == Result.OK:
						team.own_flags[service].append(f)
					else:
						team.own_flags[service].append(None)
					#t = threading.Thread(target=Push, args=(round_number, service, team))
					#t.start()
			time.sleep(2)

			# get flags
			for service in services:
				for team in teams:
					res = Result.DOWN
					f = team.own_flags[service][round_number]
					if f is not None:
						res = service.checker._pull(team.ip, f.id, f.value)
					team.services_statuses[service] = res
					if res != Result.OK:
						team.own_flags[service][round_number] = None
					else:
						team.total_own_flags.append(f.value)
						total_flags.append(f.value)
					#t = threading.Thread(target=Pull, args=(round_number, service, team))
					#t.start()

			# update 'scoreboard'
			UpdateScoreboard(round_number)

			time.sleep(2)
			#if round_number == 5: sys.exit(1)
		except Exception as ex:
			print ex
			traceback.print_exc(file=sys.stdout)
			sys.exit(1)
		finally:
			round_number += 1


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	pass
class ServiceServerHandler(SocketServer.BaseRequestHandler):
	def __init__(self, request, client_address, server):
		SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)
	def handle(self):
		print '\t\tAccepted  connection from %s' % self.client_address[0]
		try:
			s = self.request.recv(1024).strip()
			team_ip = self.client_address[0]
			team = teams_by_ip[team_ip]
			if s in team.total_own_flags:
				self.request.sendall('It\'s your own flag\n')
				return
			if s in team.stolen_flags:
				self.request.sendall('You\'ve already sent this flag\n')
				return
			if s in total_flags:
				team.stolen_flags.append(s)
				stolen_flags.append(s)
				self.request.sendall('Success\n')
			else:
				self.request.sendall('Not a flag\n')
		except Exception as ex:
			print '\t\t', str(ex)
		finally:
			print '\t\tProcessed connection from %s' % self.client_address[0]
		return

def AcceptFlag():
	server = ThreadedTCPServer((accepter_ip, accepter_port), ServiceServerHandler)
	server.serve_forever()



#
# build current scoreboard
#

def UpdateScoreboard(round_number):
	def SS(ind):
		s = ['OK', 'DOWN', 'MUMBLE', 'CORRUPT']
		return s[ind]
	table_rows = []
	for team in teams:
		# count the number of non stolen flags
		nonstolen = 0
		for service in services:
			for tof in team.own_flags[service]:
				if tof is not None and tof.value not in stolen_flags:
					nonstolen += 1
		# round_number
		stolen = len(team.stolen_flags)
		team.deffence = nonstolen
		team.attack = stolen
		team.rank = int(round(float(team.deffence + team.attack) / (len(services)*2*(round_number+1)) * 100))
		table_rows.append('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' % (team.name, SS(team.services_statuses[services[0]]),\
								SS(team.services_statuses[services[1]]), str(team.deffence), str(team.attack), str(team.rank)))
	# draw html table
	headings = '<tr><th>Team Name</th><th>post</th><th>notesup</th><th>Deffence</th><th>Attack</th><th>Rank</th></tr>'
	table = '<!DOCTYPE html><html><head><style>\
table, th, td {\
    border: 1px solid black;\
    border-collapse: collapse;\
}\
th, td {\
    padding: 5px;\
}\
</style>\
</head>\
<body>\
<table style=\"width:100%%\">\
%s</table>\
</body>\
</html>' % '\n'.join([headings] + table_rows)
	open(page_html_path, 'w+').write(table)
	return





#
# start
#

if __name__ == '__main__':
	services = [Service(post), Service(notesup)]
	teams = [Team('team1', '192.168.1.50', services), Team('team2', '192.168.1.100', services)]
	for team in teams: teams_by_ip[team.ip] = team
	t = threading.Thread(target=AcceptFlag)
	#t.start()
	PushPullLoop()
